package com.espirit.ps.ecs.bamboo.api;

public interface MyPluginComponent
{
    String getName();
}