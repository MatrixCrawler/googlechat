package com.espirit.ps.ecs.bamboo.model;

import com.espirit.ps.ecs.bamboo.Constants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * Always code as if the guy who ends up maintaining your code will be a violent psychopath that
 * knows where you live.
 *
 * <p>- John Woods
 *
 * <p>Created on 13.03.2018.
 *
 * @author brunswicker
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoogleChatRecipientConfig {
  private String webhook;

  public Map<String, Object> toHashMap() {
    HashMap<String, Object> map = new HashMap<>();
    map.put(Constants.GOOGLECHAT_WEBHOOK, webhook);
    return map;
  }
}
