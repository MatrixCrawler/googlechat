package com.espirit.ps.ecs.bamboo;

import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.espirit.ps.ecs.bamboo.model.GoogleChatRecipientConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 * Always code as if the guy who ends up maintaining your code will be a violent psychopath that
 * knows where you live.
 * <p>
 * <p>- John Woods
 * <p>
 * <p>Created on 13.03.2018.
 *
 * @author brunswicker
 */
public class GoogleChatTransport implements NotificationTransport {
    private final Logger log = LoggerFactory.getLogger(GoogleChatTransport.class);

    private final ResultsSummary resultsSummary;
    private final ApplicationProperties applicationProperties;
    private final GoogleChatRecipientConfig chatRecipientConfig;

    public GoogleChatTransport(
            ApplicationProperties applicationProperties,
            ResultsSummary resultsSummary,
            GoogleChatRecipientConfig chatRecipientConfig) {
        this.resultsSummary = resultsSummary;
        this.applicationProperties = applicationProperties;
        this.chatRecipientConfig = chatRecipientConfig;
    }

    @Override
    public void sendNotification(@NotNull Notification notification) {
        sendToChat();
    }

    public JsonObject createCard() {
        String color = getColorString();
        JsonObject message = new JsonObject();

        JsonArray cardsArray = new JsonArray();
        message.add("cards", cardsArray);

        JsonObject card = new JsonObject();
        cardsArray.add(card);
        addCardHeader(card);

        JsonArray sectionsArray = new JsonArray();
        card.add("sections", sectionsArray);

        JsonObject section = new JsonObject();
        sectionsArray.add(section);

        JsonArray widgetsArray = new JsonArray();
        section.add("widgets", widgetsArray);

        JsonObject widget = new JsonObject();
        widgetsArray.add(widget);

        JsonObject keyValue = new JsonObject();
        widget.add("keyValue", keyValue);

        keyValue.addProperty("topLabel", "Result");
        keyValue.addProperty("content", "<font color=\"" + color + "\">" + resultsSummary.getBuildState().toString() + "</font>");
        keyValue.addProperty("bottomLabel", resultsSummary.getPlanResultKey().getKey());
        JsonObject button = new JsonObject();
        JsonObject textButton = new JsonObject();
        textButton.addProperty("text", "Visit Results");
        JsonObject onClick = new JsonObject();
        JsonObject openLink = new JsonObject();
        openLink.addProperty("url", applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + "/browse/" + resultsSummary.getPlanResultKey().getKey());
        onClick.add("openLink", openLink);
        textButton.add("onClick", onClick);
        button.add("textButton", textButton);
        keyValue.add("button", button);

        addDurationSummary(widgetsArray);
        addTestSummary(widgetsArray);
        addAuthorNames(widgetsArray);
        return message;
    }

    private void addCardHeader(JsonObject card) {
        JsonObject header = new JsonObject();
        header.addProperty("title", "Bamboo Bot - Build Result");
        header.addProperty("subtitle", applicationProperties.getBaseUrl(UrlMode.ABSOLUTE));
        card.add("header", header);
    }

    private void addDurationSummary(@Nonnull JsonArray widgetsArray) {
        JsonObject durationWidget = new JsonObject();
        JsonObject durationKeyValue = new JsonObject();
        durationKeyValue.addProperty("topLabel", "Duration");
        durationKeyValue.addProperty("content", resultsSummary.getDurationDescription());
        durationWidget.add("keyValue", durationKeyValue);
        widgetsArray.add(durationWidget);
    }

    /**
     * Add a widget with the test summary
     *
     * @param widgetsArray The array to add the data to
     */
    private void addTestSummary(@Nonnull JsonArray widgetsArray) {
        JsonObject testResultWidget = new JsonObject();
        JsonObject testResultKeyValue = new JsonObject();
        testResultKeyValue.addProperty("topLabel", "Result summary");
        testResultKeyValue.addProperty("content", resultsSummary.getTestResultsSummary().getTestSummaryDescription());
        testResultKeyValue.addProperty("contentMultiline", "true");
        testResultWidget.add("keyValue", testResultKeyValue);
        widgetsArray.add(testResultWidget);
    }

    /**
     * Add a widget with comma separated list of author names
     *
     * @param widgetsArray The array to add the data to
     */
    private void addAuthorNames(JsonArray widgetsArray) {
        LinkedList<String> authorFullNames = resultsSummary
                .getUniqueAuthors()
                .stream()
                .map(author -> author.getFullName())
                .map(String::new)
                .collect(Collectors.toCollection(LinkedList::new));
        if (!authorFullNames.isEmpty()) {
            JsonObject authorWidget = new JsonObject();
            widgetsArray.add(authorWidget);
            JsonObject authorKeyValue = new JsonObject();
            authorKeyValue.addProperty("topLabel", "Authors");
            authorKeyValue.addProperty("content", String.join(", ", authorFullNames));
            authorWidget.add("keyValue", authorKeyValue);
        }
    }

    @NotNull
    private String getColorString() {
        String color;
        if (resultsSummary.getBuildState().equals(BuildState.SUCCESS)) {
            color = "#339933";
        } else if (resultsSummary.getBuildState().equals(BuildState.FAILED)) {
            color = "#ff0000";
        } else {
            color = "#F09218";
        }
        return color;
    }

    private void sendToChat() {
        Gson gson = new GsonBuilder().create();
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(chatRecipientConfig.getWebhook());
            StringEntity stringEntity = new StringEntity(gson.toJson(createCard()));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setEntity(stringEntity);
            httpClient.execute(httpPost);
        } catch (IOException e) {
            log.error("There was an error sending the message to GoogleChat", e);
        } finally {
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    log.error("There was an error closing the httpclient connection", e);
                }
            }
        }
    }
}
