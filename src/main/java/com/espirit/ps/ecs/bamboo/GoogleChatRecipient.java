package com.espirit.ps.ecs.bamboo;

import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.espirit.ps.ecs.bamboo.model.GoogleChatRecipientConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Always code as if the guy who ends up maintaining your code will be a violent psychopath that
 * knows where you live.
 *
 * <p>- John Woods
 *
 * <p>Created on 13.03.2018.
 *
 * @author brunswicker
 */
@Scanned
public class GoogleChatRecipient extends AbstractNotificationRecipient
    implements NotificationRecipient.RequiresResultSummary {

  private ApplicationProperties applicationProperties;
  private TemplateRenderer templateRenderer;

  private ResultsSummary resultsSummary;
  private GoogleChatRecipientConfig recipientConfig;

  public GoogleChatRecipient(
      @ComponentImport TemplateRenderer templateRenderer,
      @ComponentImport ApplicationProperties applicationProperties) {
    this.templateRenderer = templateRenderer;
    this.applicationProperties = applicationProperties;
    this.recipientConfig = new GoogleChatRecipientConfig();
  }

  protected GoogleChatRecipient() {

  }

  @NotNull
  @Override
  public List<NotificationTransport> getTransports() {
    return Collections.singletonList(
        new GoogleChatTransport(applicationProperties, resultsSummary, recipientConfig));
  }

  @Override
  public void setResultsSummary(@Nullable ResultsSummary resultsSummary) {
    this.resultsSummary = resultsSummary;
  }

  @NotNull
  @Override
  public String getEditHtml() {
    String editTemplate =
        ((NotificationRecipientModuleDescriptor) getModuleDescriptor()).getEditTemplate();
    if (templateRenderer == null) {
      return "No TemplateRenderer";
    }
    String renderResult = templateRenderer.render(editTemplate, recipientConfig.toHashMap());
    return renderResult != null ? renderResult : "No render result";
  }

  @Override
  public void populate(@NotNull Map<String, String[]> params) {
    recipientConfig =
        GoogleChatRecipientConfig.builder().webhook(params.get(Constants.GOOGLECHAT_WEBHOOK)[0]).build();
  }

  @NotNull
  @Override
  public String getRecipientConfig() {
    Gson gson = new GsonBuilder().create();
    return gson.toJson(recipientConfig);
  }

  @Override
  public void init(@Nullable String configurationData) {
    Gson gson = new GsonBuilder().create();
    recipientConfig = gson.fromJson(configurationData, GoogleChatRecipientConfig.class);
  }
}
