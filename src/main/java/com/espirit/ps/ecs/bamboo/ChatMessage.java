package com.espirit.ps.ecs.bamboo;

import lombok.Builder;
import lombok.Data;

/**
 * Always code as if the guy who ends up maintaining your code will be a violent psychopath that
 * knows where you live. <p> - John Woods <p> Created on 13.03.2018.
 *
 * @author brunswicker
 */
@Data
@Builder
public class ChatMessage {
    private String text;
}
