package ut.com.espirit.ps.ecs.bamboo;

import org.junit.Test;
import com.espirit.ps.ecs.bamboo.api.MyPluginComponent;
import com.espirit.ps.ecs.bamboo.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}