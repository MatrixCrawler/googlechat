package com.espirit.ps.ecs.bamboo;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.tests.TestResultsSummary;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.espirit.ps.ecs.bamboo.model.GoogleChatRecipientConfig;
import com.google.gson.JsonObject;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Always code as if the guy who ends up maintaining your code will be a violent psychopath that
 * knows where you live.
 * <p>
 * <p>- John Woods
 * <p>
 * <p>Created on 14.03.2018.
 *
 * @author brunswicker
 */
public class GoogleChatTransportTest {
    @Test
    public void test_message_return_success() {
        ResultsSummary resultsSummary = mock(ResultsSummary.class);
        GoogleChatRecipientConfig googleChatRecipientConfig = mock(GoogleChatRecipientConfig.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://BASEURL/");
        when(resultsSummary.getBuildState()).thenReturn(BuildState.SUCCESS);
        PlanResultKey planResultKey = mock(PlanResultKey.class);
        when(resultsSummary.getPlanResultKey()).thenReturn(planResultKey);
        when(planResultKey.getKey()).thenReturn("testplanresultkey");
        when(resultsSummary.getDurationDescription()).thenReturn("0h 1m 2s");
        TestResultsSummary testResultsSummary = mock(TestResultsSummary.class);
        when(resultsSummary.getTestResultsSummary()).thenReturn(testResultsSummary);
        when(testResultsSummary.getTestSummaryDescription()).thenReturn("Pretty Description");
        GoogleChatTransport googleChatTransport =
                new GoogleChatTransport(applicationProperties, resultsSummary, googleChatRecipientConfig);
        JsonObject card = googleChatTransport.createCard();
        assertEquals("{\"cards\":[{\"header\":{\"title\":\"Bamboo Bot - Build Result\",\"subtitle\":\"http://BASEURL/\"},\"sections\":[{\"widgets\":[{\"keyValue\":{\"topLabel\":\"Result\",\"content\":\"<font color=\\\"#339933\\\">Successful</font>\",\"bottomLabel\":\"testplanresultkey\",\"button\":{\"textButton\":{\"text\":\"Visit Results\",\"onClick\":{\"openLink\":{\"url\":\"http://BASEURL//browse/testplanresultkey\"}}}}}},{\"keyValue\":{\"topLabel\":\"Duration\",\"content\":\"0h 1m 2s\"}},{\"keyValue\":{\"topLabel\":\"Result summary\",\"content\":\"Pretty Description\",\"contentMultiline\":\"true\"}}]}]}]}", card.toString());
    }

    @Test
    public void test_message_return_unsuccessful() {
        ResultsSummary resultsSummary = mock(ResultsSummary.class);
        GoogleChatRecipientConfig googleChatRecipientConfig = mock(GoogleChatRecipientConfig.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://BASEURL/");
        when(resultsSummary.getBuildState()).thenReturn(BuildState.FAILED);
        PlanResultKey planResultKey = mock(PlanResultKey.class);
        when(resultsSummary.getPlanResultKey()).thenReturn(planResultKey);
        when(planResultKey.getKey()).thenReturn("testplanresultkey");
        when(resultsSummary.getDurationDescription()).thenReturn("0h 1m 2s");
        TestResultsSummary testResultsSummary = mock(TestResultsSummary.class);
        when(resultsSummary.getTestResultsSummary()).thenReturn(testResultsSummary);
        when(testResultsSummary.getTestSummaryDescription()).thenReturn("Pretty Description");
        GoogleChatTransport googleChatTransport =
                new GoogleChatTransport(applicationProperties, resultsSummary, googleChatRecipientConfig);
        JsonObject card = googleChatTransport.createCard();
        assertEquals("{\"cards\":[{\"header\":{\"title\":\"Bamboo Bot - Build Result\",\"subtitle\":\"http://BASEURL/\"},\"sections\":[{\"widgets\":[{\"keyValue\":{\"topLabel\":\"Result\",\"content\":\"<font color=\\\"#ff0000\\\">Failed</font>\",\"bottomLabel\":\"testplanresultkey\",\"button\":{\"textButton\":{\"text\":\"Visit Results\",\"onClick\":{\"openLink\":{\"url\":\"http://BASEURL//browse/testplanresultkey\"}}}}}},{\"keyValue\":{\"topLabel\":\"Duration\",\"content\":\"0h 1m 2s\"}},{\"keyValue\":{\"topLabel\":\"Result summary\",\"content\":\"Pretty Description\",\"contentMultiline\":\"true\"}}]}]}]}", card.toString());
    }

    @Test
    public void test_message_return_unknown() {
        ResultsSummary resultsSummary = mock(ResultsSummary.class);
        GoogleChatRecipientConfig googleChatRecipientConfig = mock(GoogleChatRecipientConfig.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://BASEURL/");
        when(resultsSummary.getBuildState()).thenReturn(BuildState.UNKNOWN);
        PlanResultKey planResultKey = mock(PlanResultKey.class);
        when(resultsSummary.getPlanResultKey()).thenReturn(planResultKey);
        when(planResultKey.getKey()).thenReturn("testplanresultkey");
        when(resultsSummary.getDurationDescription()).thenReturn("0h 1m 2s");
        TestResultsSummary testResultsSummary = mock(TestResultsSummary.class);
        when(resultsSummary.getTestResultsSummary()).thenReturn(testResultsSummary);
        when(testResultsSummary.getTestSummaryDescription()).thenReturn("Pretty Description");
        GoogleChatTransport googleChatTransport =
                new GoogleChatTransport(applicationProperties, resultsSummary, googleChatRecipientConfig);
        JsonObject card = googleChatTransport.createCard();
        assertEquals("{\"cards\":[{\"header\":{\"title\":\"Bamboo Bot - Build Result\",\"subtitle\":\"http://BASEURL/\"},\"sections\":[{\"widgets\":[{\"keyValue\":{\"topLabel\":\"Result\",\"content\":\"<font color=\\\"#F09218\\\">Unknown</font>\",\"bottomLabel\":\"testplanresultkey\",\"button\":{\"textButton\":{\"text\":\"Visit Results\",\"onClick\":{\"openLink\":{\"url\":\"http://BASEURL//browse/testplanresultkey\"}}}}}},{\"keyValue\":{\"topLabel\":\"Duration\",\"content\":\"0h 1m 2s\"}},{\"keyValue\":{\"topLabel\":\"Result summary\",\"content\":\"Pretty Description\",\"contentMultiline\":\"true\"}}]}]}]}", card.toString());
    }

    @Test
    public void test_message_return_success_with_authors() {
        ResultsSummary resultsSummary = mock(ResultsSummary.class);
        GoogleChatRecipientConfig googleChatRecipientConfig = mock(GoogleChatRecipientConfig.class);
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE)).thenReturn("http://BASEURL/");
        when(resultsSummary.getBuildState()).thenReturn(BuildState.SUCCESS);
        PlanResultKey planResultKey = mock(PlanResultKey.class);
        when(resultsSummary.getPlanResultKey()).thenReturn(planResultKey);
        when(planResultKey.getKey()).thenReturn("testplanresultkey");
        when(resultsSummary.getDurationDescription()).thenReturn("0h 1m 2s");
        TestResultsSummary testResultsSummary = mock(TestResultsSummary.class);
        when(resultsSummary.getTestResultsSummary()).thenReturn(testResultsSummary);
        when(testResultsSummary.getTestSummaryDescription()).thenReturn("Pretty Description");
        Author author = mock(Author.class);
        Author author1 = mock(Author.class);
        when(resultsSummary.getUniqueAuthors()).thenReturn(new LinkedHashSet<>(Arrays.asList(author, author1)));
        when(author.getFullName()).thenReturn("AuthorsFullName");
        when(author1.getFullName()).thenReturn("AuthorsFullName1");
        GoogleChatTransport googleChatTransport =
                new GoogleChatTransport(applicationProperties, resultsSummary, googleChatRecipientConfig);
        JsonObject card = googleChatTransport.createCard();
        assertEquals("{\"cards\":[{\"header\":{\"title\":\"Bamboo Bot - Build Result\",\"subtitle\":\"http://BASEURL/\"},\"sections\":[{\"widgets\":[{\"keyValue\":{\"topLabel\":\"Result\",\"content\":\"<font color=\\\"#339933\\\">Successful</font>\",\"bottomLabel\":\"testplanresultkey\",\"button\":{\"textButton\":{\"text\":\"Visit Results\",\"onClick\":{\"openLink\":{\"url\":\"http://BASEURL//browse/testplanresultkey\"}}}}}},{\"keyValue\":{\"topLabel\":\"Duration\",\"content\":\"0h 1m 2s\"}},{\"keyValue\":{\"topLabel\":\"Result summary\",\"content\":\"Pretty Description\",\"contentMultiline\":\"true\"}},{\"keyValue\":{\"topLabel\":\"Authors\",\"content\":\"AuthorsFullName, AuthorsFullName1\"}}]}]}]}", card.toString());
    }
}
